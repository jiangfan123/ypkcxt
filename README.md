# 药库工作站管理系统

#### 项目介绍
该项目为药库工作站管理系统，通过本系统软件，能帮助库存管理人员利用计算机，快速方便的对药品进行管理、进货、出货、查找的所需操作,智能化功能及对各种特殊药品的操作。 大大节省了医院药品仓库的人力、物力和财力。 该系统工作在windows系统平台，以SQL数据库为基础
#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)